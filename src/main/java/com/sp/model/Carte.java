package com.sp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Carte {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String family_name;
	private String img_src;
	private String name;
	private String description;
	private int hp;
	private int energy;
	private int attack;
	private int defense;
	private int price;
	private Integer idOwner;


	public Carte() {}
	/**
	 * But Constructeur d'une carte
	 * @param family_name
	 * @param img_src
	 * @param name
	 * @param description
	 * @param hp
	 * @param energy
	 * @param attack
	 * @param defense
	 * @param price
	 */
	public Carte(String family_name, String img_src,String name,String description,int hp,int energy,int attack,int defense, int price) {
		super();
		this.family_name = family_name;
		this.img_src = img_src;
		this.name = name;
		this.description = description;
		this.hp = hp;
		this.energy = energy;
		this.attack = attack;
		this.defense = defense;
		this.price = price;
		this.idOwner = null;
	}

	/**
	 * But : renvoyer une chaine de caractère contenant les attributs de Carte
	 * @param 
	 * @return la chaine de caractère contenant les attributs de Carte
	 */
	@Override
	public String toString() {
		return "{id:" + id + ", family_name:" + family_name + ", img_src:" + img_src + ", name:" + name
				+ ", description:" + description + ", hp:" + hp + ", energy:" + energy + ", attack:" + attack
				+ ", defense:" + defense + ", price:" + price + "}";
	}

	/**
	 * But : récupère l'id de carte
	 * @param 
	 * @return l'id de la carte
	 */
	public int getId() {
		return id;
	}

	/**
	 * But : récupère la famille de la carte
	 * @param 
	 * @return la famille de la carte
	 */
	public String getFamily_name() {
		return family_name;
	}
	
	/**
	 * But : modifie la famille de la carte
	 * @param family_name la nouvelle famille de la carte
	 * @return /
	 */
	public void setFamily_name(String family_name) {
		this.family_name = family_name;
	}

	/**
	 * But : récupère la source de l'image de la carte
	 * @param 
	 * @return la source de l'image de la carte
	 */
	public String getImg_src() {
		return img_src;
	}

	/**
	 * But :  modifie la source de l'image de la carte
	 * @param img_src la nouvelle source de l'image de la carte
	 * @return /
	 */
	public void setImg_src(String img_src) {
		this.img_src = img_src;
	}
	
	/**
	 * But : récupère le nom de la carte
	 * @param 
	 * @return le nom de la carte
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * But : modifie le nom de la carte
	 * @param name le nouveau nom de la carte
	 * @return /
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * But : récupère la description de la carte
	 * @param 
	 * @return la description de la carte
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * But : modifie la description de la carte
	 * @param description la nouvelle description de la carte
	 * @return /
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * But : récupère les points de vie de la carte
	 * @param 
	 * @return les points de vie de la carte
	 */
	public int getHp() {
		return hp;
	}
	
	/**
	 * But : modifie les points de vie de la carte
	 * @param hp les nouveaux points de vie de la carte
	 * @return /
	 */
	public void setHp(int hp) {
		this.hp = hp;
	}
	
	/**
	 * But : récupère l'énergie de la carte
	 * @param 
	 * @return l'énergie de la carte
	 */
	public int getEnergy() {
		return energy;
	}

	/**
	 * But : modifie l'énergie de la carte
	 * @param energy la nouvelle énergie de la carte
	 * @return /
	 */
	public void setEnergy(int energy) {
		this.energy = energy;
	}

	/**
	 * But : récupère la valeur d'attaque de la carte
	 * @param 
	 * @return la valeur d'attaque de la carte
	 */
	public int getAttack() {
		return attack;
	}
	
	/**
	 * But : modifie la valeur d'attaque de la carte 
	 * @param attack la nouvelle valeur d'attaque de la carte
	 * @return /
	 */
	public void setAttack(int attack) {
		this.attack = attack;
	}

	/**
	 * But : récupère la valeur de défense de la carte
	 * @param 
	 * @return la valeur de défense de la carte
	 */
	public int getDefense() {
		return defense;
	}

	/**
	 * But : modifie la valeur de défense de la carte
	 * @param defense la nouvelle valeur de défense de la carte
	 * @return /
	 */
	public void setDefense(int defense) {
		this.defense = defense;
	}

	/**
	 * But : récupère le prix de la carte
	 * @param 
	 * @return le prix de la carte
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * But : modifie le prix de la carte
	 * @param price le nouveau prix de la carte
	 * @return /
	 */
	public void setPrice(int price) {
		this.price = price;
	}
	
	/**
	 * But : donne le propriétaire de la carte
	 * @param price le nouveau prix de la carte
	 * @return /
	 */
	public Integer getIdOwner() {
		return idOwner;
	}
	
	/**
	 * But : modifie le propriétaire de la carte
	 * @param price le nouveau propriétaire de la carte
	 * @return /
	 */
	public void setIdOwner(int idOwner) {
		this.idOwner = idOwner;
	}
}