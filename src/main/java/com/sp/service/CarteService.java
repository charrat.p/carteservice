package com.sp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sp.model.Carte;
import com.sp.repository.CarteRepository;

@Service
public class CarteService {
	
	@Autowired
	CarteRepository cRepository;
	
	/**
	 * But :Ajout d'une carte dans le repository
	 * @param Carte 
	 */
	public void addCarte(Carte c) {
		Carte createdCarte=cRepository.save(c);
		System.out.println(createdCarte);
	}

	/**
	 * But : Retours de la liste de toutes les cartes 
	 * @return List<Carte>
	 */
	public List<Carte> getAllCartes() {
		List<Carte> cartes = new ArrayList<>();
		cRepository.findAll().forEach(cartes::add);
	    return cartes;
	}
	
	/**
	 * But : Retour de la carte via son id
	 * @param id
	 * @return
	 */
	public Carte getCarteById(int id) {
		Optional<Carte> cOpt =cRepository.findById(id);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}
	
	/**
	 * But : Retour d'une carte par son nom 
	 * @param String Search
	 * @return Carte 
	 */
	public Carte getCardByName(String search) {
		Carte carteReturn = new Carte("Inconnu","Inconnu","Inconnu","Inconnu",0,0,0,0,0);
		List<Carte> cartes = new ArrayList<>();
		cRepository.findAll().forEach(cartes::add);
		for (int j=0;j<cartes.size();j++) {
			Carte ctest = cartes.get(j);
			if (ctest.getName().equals(search)) carteReturn = ctest;
		}
	    return carteReturn;
	}
	
	/**
	 * But : Modification du propriétaire d'une carte
	 * @param idCarte
	 * @param idOwner
	 */
	public void modificatOwner(int idCarte, int idOwner) {
		Optional<Carte> cOpt =cRepository.findById(idCarte);
		if (cOpt.isPresent()) {
			cOpt.get().setIdOwner(idOwner);
		}
	}
	
	/**
	 * But : Récupérer le prix d'une carte 
	 * @param id
	 * @return price 
	 */
	public int getPriceCarteById(int id) {
		Optional<Carte> cOpt =cRepository.findById(id);
		if (cOpt.isPresent()) {
			return cOpt.get().getPrice();
		}else {
			return 0;
		}
	}
	
	/**
	 * But : Modification du propriétaire de la carte
	 * @param idCarte
	 * @param idOwner
	 */
	public void modificateOwner(int idCarte, Integer idOwner) {
		Carte carte = getCarteById(idCarte);
		carte.setIdOwner(idOwner);
	}
	
	/**
	 * But : Création d'une liste de 5 cartes  
	 * @param idOwner
	 * @return
	 */
	public List<Carte> getCinqCartes(Integer idOwner) {
		List<Carte> cartes = new ArrayList<>();
		List<Carte> cartesARetourner = new ArrayList<>();
		List<Integer> idCartajoute = new ArrayList<Integer>();
		cRepository.findAll().forEach(cartes::add);
		int idMax = cartes.size()-1;
		int idtest;
		int j=0;
		while ( j<5) {
			idtest = genererInt(1,idMax);
			if (!(idCartajoute.contains(idtest)) && (cartes.get(idtest).getIdOwner() == null)) {
				idCartajoute.add(cartes.get(idtest).getId());
				cartesARetourner.add(cartes.get(idtest));
				cartes.get(idtest).setIdOwner(idOwner);
				cRepository.save(cartes.get(idtest));
				j +=1;
			} 
		}
 	    return cartesARetourner;
	}
	
	/** 
	 * But : Fonction pour faure un random dans la liste
	 * @param borneInf
	 * @param borneSup
	 * @return
	 */
	private int genererInt(int borneInf, int borneSup){
		   Random random = new Random();
		   int nb;
		   nb = borneInf+random.nextInt(borneSup-borneInf);
		   return nb;
	}
}